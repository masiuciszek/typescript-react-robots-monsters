import React from 'react';
import axios from 'axios';
import './App.css';
import styled from 'styled-components';
import CardList from './components/CardList';
import { Monster } from './types';
import SearchInput from './components/SearchInput';

const AppStyles = styled.main`
  padding: 1rem;
  height: 100%;
  h1{
    text-align: center;
    padding: 1.5rem;
    color: #feeeee;
    font-family: 'Shadows Into Light', cursive;
    font-size: 6rem;
    border-bottom: 1px solid #feeeee;
    width: 70%;
    margin: 0 auto;
  }
  input{
    display: block;
    margin: 2rem auto;
    width: 24rem;
    font-size: 1.9rem;
    padding: 2rem
  }
`;

const App: React.FC = () => {
  const [monsters, setMonsters] = React.useState([]);
  const [search, setSearch] = React.useState<string>('');

  React.useEffect(() => {
    axios.get('https://jsonplaceholder.typicode.com/users').then((res) => setMonsters(res.data));
  }, []);

  const handleSearch = (e: React.ChangeEvent<HTMLInputElement>) => {
    setSearch(e.target.value);
  };

  const filtredMonsters = monsters.filter((monster: Monster) => monster.name.toLowerCase().includes(search.toLowerCase()));


  return (
    <AppStyles>
      <h1>Robots Monsters</h1>
      <SearchInput search={search} handleSearch={handleSearch} />
      <CardList monsters={filtredMonsters} />
    </AppStyles>
  );
};

export default App;
