/* eslint-disable react/prop-types */
import * as React from 'react';
import styled from 'styled-components';
import { Monster } from '../types';

interface Props {
  monster: Monster;
}

const StyledMonster = styled.div`
  background: rgba(0,0,0,0.4);
  color: #fff;
  display: flex;
  flex-direction: column;
  border: 1px solid #fff;
  padding: 2.5rem;
  border-radius: .6rem;
  transition: 300ms ease-in-out;
  backface-visibility: hidden;
  transform: translateZ(0);
  cursor: pointer;
    &:hover{
      transform: scale(1.05);
    }
`;

const Card: React.FC<Props> = ({ monster }) => {
  let a;
  return (
    <StyledMonster>
      <img src={`https://robohash.org/${monster.id}?set=set3&size=130x130`} alt="" />
      <h3>{monster.name}</h3>
    </StyledMonster>
  );
};
export default Card;
