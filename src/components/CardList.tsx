import * as React from 'react';
import styled from 'styled-components';
import { Monster } from '../types';
import Card from './Card';

const StyledList = styled.div`
  padding: 1rem;
  display: grid;
  grid-template-columns: repeat(3, 1fr);
  grid-template-rows: auto;
  grid-gap: 2rem;
  margin: 0 auto;
  width: 85vw;
  @media(max-width: 790px){
    grid-template-columns: repeat( auto-fit, minmax(250px, 1fr) );
  }
`;


interface Props {
  monsters: Monster[];
}

const CardList: React.FC<Props> = ({ monsters }) => {
  let a;
  return (
    <StyledList>
      {monsters.map((monster) => (
        <Card key={monster.id} monster={monster} />
      ))}
    </StyledList>
  );
};
export default CardList;
