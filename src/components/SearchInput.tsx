/* eslint-disable react/prop-types */
import * as React from 'react';
import styled from 'styled-components';

interface Props {
  search: string;
  handleSearch: (e: React.ChangeEvent<HTMLInputElement>) => void;

}

const StyledSearch = styled.input.attrs({ type: 'search' })`
    display: block;
    margin: 3rem auto;
    width: 34rem;
    font-size: 2rem;
    padding: .3rem 1.2rem;
    border: 2px solid #222;
    transition: 300ms ease-in-out;
    &:focus{
      transform: scale(1.22);
    }
`;

const SearchInput: React.FC<Props> = ({ search, handleSearch }) => (<StyledSearch placeholder="Search for a monster robot" value={search} onChange={handleSearch} />);
export default SearchInput;
