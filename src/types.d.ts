
export type Monster = {
  id: number;
  name: string;
  username: string;
  email: string;
  address: Address;
  geo: Geo;
  phone: string;
  website: string;
  company: Company;
}

type Address = {
  street: string;
  suite: string;
  city: string;
  zipcode: string;
}

type Geo = {
  lat: string;
  lng: string;
}

type Company = {
  name: string;
  catchPhrase: string;
  bs: string;
};
